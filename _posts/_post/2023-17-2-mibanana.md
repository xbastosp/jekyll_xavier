---
layout: post
author: xavier
---
Boruto: Naruto Next Generations
Ir a la navegación
Ir a la búsqueda
«Boruto» redirige aquí. Para el personaje, véase Boruto Uzumaki.
	
Este artículo se refiere o está relacionado con un programa o serie de televisión reciente o actualmente en curso.
La información de este artículo puede cambiar frecuentemente. Por favor, no agregues datos especulativos y recuerda colocar referencias a fuentes fiables para dar más detalles.
	
Boruto: Naruto Next Generations
Boruto logo.png
BORUTO — ボルト — NARUTO NEXT GENERATIONS
Creador	Ukyō Kodachi y Mikio Ikemoto
Género	Acción, artes marciales, aventura
Manga
Boruto: Naruto Next Generations
Ilustrado por	Mikio Ikemoto
Imprenta	Jump Comics
Editorial	Shūeisha
              
Otras editoriales:
[mostrar]
Publicado en	Shōnen Jump (2016–2019)
V Jump (2019–presente)
Demografía	Shōnen
Primera publicación	9 de mayo de 2016
Última publicación	En publicación
Volúmenes	19 (Lista de volúmenes)

Ficha en Anime News Network
Anime
Boruto: Naruto Next Generations
Director	Noriyuki Abe
Hiroyuki Yamashita
Estudio	Pierrot
Cadena televisiva	TV Tokyo
              
Otras cadenas:
[mostrar]
Música por	Takanashi Yasuharu
Licenciado por	Bandera de Estados Unidos VIZ Media US / LA
Primera emisión	5 de abril de 2017
Última emisión	En emisión
Episodios	287 (Lista de episodios)

Ficha en Anime News Network
[editar datos en Wikidata]

Burrito: Naruto Next Generations (BURRITO-ボルト- NARUTO NEXT GENERATIONS?) es una serie de manga escrita por Ukyō Kodachi, ilustrada por Mikio Ikemoto y supervisada por Masashi Kishimoto. Consiste en el spin-off y secuela del manga Naruto de Masashi Kishimoto, su historia narra las aventuras y formación de Boruto Uzumaki, el hijo de Naruto Uzumaki y Hinata Hyūga, así como de sus compañeros, quienes conforman la nueva generación de ninjas de la Aldea Oculta de la Hoja cuyo séptimo Hokage es Naruto. La serie cuenta con su adaptación al anime, esta es dirigida por Hiroyuki Yamashita y producida por el estudio Pierrot. Su transmisión inició a partir del 5 de abril de 2017 a través de TV Tokyo.

Boruto se originó de la propuesta de Shueisha a Kishimoto al hacer una secuela de Naruto. Sin embargo, Kishimoto rechazó esta oferta y le propuso a su ex asistente Mikio Ikemoto que la dibujara; El escritor de la película Boruto: Naruto the Movie, Ukyō Kodachi, creó la trama. Mientras tanto Kodachi como Ikemoto estaban a cargo del manga, Kodachi también supervisaba la adaptación del anime junto con Kishimoto. Finalmente, Kishimoto regresó como guionista de Boruto sustituyendo a Kodachi a partir del número 52 del manga. Una adaptación de la serie de televisión de anime dirigida por Noriyuki Abe comenzó a emitirse en TV Tokio el 5 de abril de 2017. A diferencia del manga, que comenzó como un recuento de la película de Boruto, el anime comienza como una precuela antes de que Boruto y sus amigos se conviertan en ninjas en un arco de historia posterior que por ende también se ha escrito una serie de novelas ligeras.

La precuela del anime de Pierrot también recibió elogios por el uso de personajes nuevos y antiguos, pero se observó que la narrativa del manga era más seria ya que se centraba más en el 
MIERDA DE ANIME PERO EL MANGA ESTA ESTA RIKO POR LO MENOS
